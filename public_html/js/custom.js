
$(function () {

    $('#myTab a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    });
    
    // default
    $('.tab1_data').show();
    $('.tab2_data').hide();
    
    
    $('#clc_szakember').click(function (e) {
       e.preventDefault();
       $('.tab1_data').show();
       $('.tab2_data').hide();
    });
    $('#clc_munka').click(function (e) {
       e.preventDefault();
       $('.tab2_data').show();
       $('.tab1_data').hide();
    });
    
    $('#ex1').slider({});
    $('#ex2').slider({});
    $('#ex3').slider({});
    $('#ex4').slider({});

    $("#searchbox_m_slider_1").slider({});
    
    
    $('#myCarousel').carousel({
	interval: 10000
    });
    
    $('#szakember_slider').carousel({
	interval: 10000
    });
    
    $('#jumbo-worker-pic').hide();
    $('#jumbo-how-to-work').hide();
    $('#jumbo-left-place').hide();
    $('#jumbo-right-place').hide();

    $('#jumbo-left-place').delay(1000)
      .queue(function() {
       $(this).show();
       $(this).addClass("animation-fadeInRight");
       $(this).dequeue();
      });

    $('#jumbo-right-place').delay(2000)
      .queue(function() {
	$(this).show();
	$(this).addClass("animation-fadeInLeft");
	$(this).dequeue();
      });


    
    $('#jumbo-how-to-work').delay(3000)
      .queue(function() {
       $(this).show();
       $(this).addClass("animation-fadeInQuick");
       $(this).dequeue();
      });
    
      
  $('.slider1').bxSlider({
    slideWidth: 210,
    minSlides: 1,
    maxSlides: 5,
    slideMargin: 15,
    nextSelector: '#slider-bx-next',
    prevSelector: '#slider-bx-prev',
    prevText: '<span><i class="fa fa-chevron-left"></i></span>',
    nextText: '<span><i class="fa fa-chevron-right"></i></span>',
    pager: false
  });

  $('.slider2').bxSlider({
    slideWidth: 210,
    minSlides: 1,
    maxSlides: 5,
    slideMargin: 15,
    nextSelector: '#slider-bx-next2',
    prevSelector: '#slider-bx-prev2',
    prevText: '<span><i class="fa fa-chevron-left"></i></span>',
    nextText: '<span><i class="fa fa-chevron-right"></i></span>',
    pager: false
  });
    
  // Initialize Tags Input
  $('.input-tags').tagsInput({ width: 'auto', height: 'auto'});
  
  
  // Initialize Chosen
  $('.select-chosen').chosen({width: "100%"});

  // Initialize Select2
  $('.select-select2').select2();
  
  
  $('.munkak-table-hovertext').hide();
  $('.szakember-lista-opt-dn-btn').hide();
  
  /*$('.munkak-table-datarow').hover(function() {
    $('.munkak-table-hovertext').hide(); // close ALL opened hovertext!
    $(this).next('.munkak-table-hovertext').show(); // show only ONE! ;)
    $('.szakember-lista-opt-dn-btn').hide();
    $(this).find('.szakember-lista-opt-dn-btn').show();
  });*/
  
  $('.munkak-table-datarow').hover(function() {
      $(this).next('.munkak-table-hovertext').show('slow');
      $(this).find('.szakember-lista-opt-dn-btn').show('fast');
    },function(){
      $(this).next('.munkak-table-hovertext').hide('fast');
      $(this).find('.szakember-lista-opt-dn-btn').hide('normal');
    });
  
  $('#a-munka-tabla tr').addClass('munkak-table-even');
  $("#a-munka-tabla tr:nth-child(4n)")
    .add("#a-munka-tabla tr:nth-child(4n-1)")
    .removeClass('munkak-table-even')
    .addClass("munkak-table-odd");
  
  $('#a-munka-tabla2 tr:even').addClass('munkak-table-even');
  $('#a-munka-tabla2 tr:odd').addClass('munkak-table-odd');
  
  
  $('.mr-rp-rpbox').addClass('active2').slideUp();
  
  $('#mr-show-reply').click( function(e) {
    if ($('.mr-rp-rpbox').hasClass('active2')) {
      $('.mr-rp-rpbox').removeClass('active2').slideDown();
    } else {
      $('.mr-rp-rpbox').addClass('active2').slideUp();
    }
  });

  // munkak - review buttons
  $('#mp-btn-all').click( function(e) {
    $('.mp-review-i-am-good-review').show();
    $('.mp-review-i-am-bad-review').show();
  });
  $('#mp-btn-good').click( function(e) {
    $('.mp-review-i-am-good-review').show();
    $('.mp-review-i-am-bad-review').hide();
  });
  $('#mp-btn-bad').click( function(e) {
    $('.mp-review-i-am-good-review').hide();
    $('.mp-review-i-am-bad-review').show();
  });



});


