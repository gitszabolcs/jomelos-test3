Dropzone.autoDiscover = false;

$(function(){

	$("abbr.timeago").timeago();
    $(".countdown").kkcountdown({
        dayText     : 'nap ',
        daysText    : 'nap ',
        hoursText   : ':',
        minutesText : ':',
        secondsText : '',
        displayZeroDays : true,
        callback    : function(element){
        	console.log(element);
        },
        rusNumbers  :   false
    });
    /*
        $('#basic-wizard .select-chosen').chosen({width: "98%","min-width":"200px"}).change(function(e) {
                    $('#basic-wizard').bootstrapValidator('revalidateField', "color");
                });
    */
    $('.select-chosen').chosen({width: "98%","min-width":"200px"});
});

var $formlogin,
    $formreminder,
    $formregister,
    $formregistersuccess,
    $this,
    $formloginmodal,
    $formloginmodal,
    $formgiveoffer,
    $formgiveedit,
    $dropzone;
var Jomelos = function() {
    var e = function(e, r, s) {
        e.slideUp(250), r.slideDown(250, function() {
            $("input").placeholder()
        }), window.location = s ? "#" + s : "#"
    };
    return {
        LoggedIn: function(){
                $(function(){

                    Dropzone.options.dropzoneadd = {
                        paramName: "file",
                        url:"/ajax/fileupload",
                        dictRemoveFile:"Törlés",
                        maxFilesize: 2,
                        addRemoveLinks :"Törlés",
                        dictCancelUpload:"Törlés",
                        maxFiles: 5,
                        parallelUploads:5,
                        uploadMultiple: true,
                        autoProcessQueue : false,    
                    };
                    if($("#dropzoneadd").length > 0)
                        var $dropzone = new Dropzone("#dropzoneadd");
                    $(".radio-cost-type").on("change",function(){
                        if($(this).val() == "fix"){
                            $('#hour').fadeOut(300);
                            $('#fix').delay(300).fadeIn(300);
                        }
                        else{
                            $('#fix').fadeOut(300);
                            $('#hour').delay(300).fadeIn(300);
                        }
                    })
                    $('.radio-hour-custom').on("change",function(){
                        if($(this).val() == "nemtudom"){
                            $('#hd1').attr("disabled","disabled");
                            $('#d1').attr("disabled","disabled");
                        }
                        else{
                            $('#hd1').removeAttr("disabled");
                            $('#d1').removeAttr("disabled");
                        }
                    });
                    $.validator.setDefaults({ ignore: ":hidden:not(select)" });
                    $('#basic-wizard').on("submit",function(){
                            if($(this).valid()){
                                $dropzone.processQueue();
                                $dropzone.on("successmultiple", function(file,response) {
                                    if(response == "1"){
                                        $values = $('#basic-wizard').serialize();
                                        $.post("/ajax/new_job",$values,function(data){
                                            if(data.success)
                                                top.location.href = data.redirect;
                                        },"json");
                                    }
                                    return false;
                                });
                                return false;
                            }
                    });
                    $('#basic-wizard').formwizard({
                        disableUIStyles: true,
                        textNext :"Következő",
                        textBack :"Előző",
                        step_shown: function(event,data){
                            alert("asd");
                        },
                        validationEnabled: true,
                        validationOptions: {
                            errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                            errorElement: 'span',
                            errorPlacement: function(error, e) {
                                e.parents('.form-group > div').append(error);
                            },
                            highlight: function(e) {
                                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                                $(e).closest('.help-block').remove();
                            },
                            success: function(e) {
                                // You can use the following if you would like to highlight with green color the input after successful validation!
                                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                                e.closest('.help-block').remove();
                            },
                            rules: {
                                job_title: {
                                    required: true,
                                    minlength: 5
                                },
                                job_description: {
                                    required: true,
                                    minlength: 60
                                },
                                "job_skills[]":{
                                    required:true,
                                    minlength:1,
                                    maxlength:7
                                },
                                "work_sites[]":{
                                    required:true,
                                    minlength:1,
                                    maxlength:5,
                                },
                                "time-duration":{
                                    required:true,
                                    minlength:1,
                                    maxlength:10
                                }
                            },
                            messages: {
                                job_title: {
                                    required: 'Kérlek írd be a munka címét!',
                                    minlength: 'A munka címe minimum 5 karakterből kell, hogy álljon.'
                                },
                                job_description: {
                                    required: 'Kérlek írd be a munka leírását.',
                                    minlength: 'A munka leírása minimum 60 karakterből kell, hogy álljon.'
                                },
                                "job_skills[]":{
                                    required:"Minimum egyet válassz a listából.",
                                    maxlength:"Maximum 7 szakterületet választhatsz ki."
                                },
                                "work_sites[]":{
                                    required:"A Munka helyszínét kötelező megadni.",
                                    maxlength:"Maximum 5 helyszínt választhatsz ki."
                                },
                                "time-duration":{
                                    required:"A hírdetés időtartamát kötelező megadni.",
                                    maxlength:"Maximum 10 nap lehet az időtartam.",
                                    minlength:"Minimum 1 nap."
                                },
                                default: {
                                    required: 'Please provide a password',
                                    minlength: 'Your password must be at least 5 characters long',
                                    equalTo: 'Please enter the same password as above'
                                }
                            }
                        },
                        inDuration: 0,
                        outDuration: 0
                    });
                });
            },
            Init: function(){
                $(function(){
                    $('body').removeClass('page-loading');
                });
                $formloginmodal = $('#form-login-modal').validate({
                    errorClass: "help-block animation-slideDown",
                    errorElement: "div",
                    errorPlacement: function(e, r) {
                        r.parents(".form-group > div").append(e)
                    },
                    highlight: function(e) {
                        $(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                    },
                    success: function(e) {
                        e.closest(".form-group").removeClass("has-success has-error"), e.closest(".help-block").remove()
                    },
                    rules: {
                        "username": {
                            required: !0
                        },
                        "password": {
                            required: !0
                        }
                    },
                    messages: {
                        "username": {
                            required: "A felhasználó nevet vagy az emailcímet kötelező megadni."
                        },
                        "password": {
                            required: "Kérlek töltsd ki a jelszó mezőt is."
                        }
                    }
                });
                $('#form-login-modal').on("submit",function(){
                        if($(this).valid()){
                            $values = $(this).serialize();
                            $.post("/ajax/login",$values,function(data){
                                if(data.status == "0"){
                                    $formloginmodal.showErrors({"password": "Helytelen jelszót adtál meg."});
                                }else{
                                    top.location.href = "";
                                }
                            },"json");
                        }
                        return false;
                });
        },
        munka: function(){
            Dropzone.options.dropzoneable = {
              paramName: "file",
              url:"/ajax/fileupload",
              dictRemoveFile:"Törlés",
              maxFilesize: 2,
              addRemoveLinks :"Törlés",
              dictCancelUpload:"Törlés",
              maxFiles: 5,
              parallelUploads:5,
              uploadMultiple: true,
              autoProcessQueue : false,    
            };
            if($("#dropzoneable").length > 0)
                var $dropzone = new Dropzone("#dropzoneable");
            $formgiveedit = $('#form-modal-szerkesztes').validate({
                errorClass: "help-block animation-slideDown",
                errorElement: "div",
                errorPlacement: function(e, r) {
                    r.parents(".form-group > div").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".form-group").removeClass("has-success has-error"), e.closest(".help-block").remove()
                },
                rules: {
                    "title": {
                        required: !0
                    },
                    "description": {
                        required: !0
                    }
                },
                messages: {
                    "title": {
                        required: "A címet kötelező megadni!"
                    },
                    "description": {
                        required: "A leírást kötelező megadni!"
                    }
                }
            });
            $('#form-modal-szerkesztes').on("submit",function(){
                    if($(this).valid()){
                        $dropzone.processQueue();
                        $dropzone.on("successmultiple", function(file,response) {
                            if(response == "1"){
                                $values = $('#form-modal-szerkesztes').serialize();
                                $.post("/ajax/edit",$values,function(data){
                                    if(data.success)
                                        eval(data.javascript);
                                },"json");
                            }
                            return false;
                        });
                        return false;
                    }
            });
            $formgiveoffer = $('#form-modal-ajanlat').validate({
                errorClass: "help-block animation-slideDown",
                errorElement: "div",
                errorPlacement: function(e, r) {
                    r.parents(".form-group > div").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".form-group").removeClass("has-success has-error"), e.closest(".help-block").remove()
                },
                rules: {
                    "price": {
                        required: !0
                    },
                    "days": {
                        required: !0
                    },
                    "checkbox": {
                        required: !0
                    }
                },
                messages: {
                    "price": {
                        required: "Az összeget kötelező megadni!"
                    },
                    "days": {
                        required: "A napot kötelező megadni!"
                    },
                    "rules": {
                        required: "kötelező elfogadni a felhasználási feltételeket!"
                    }
                }
            });
            $('#form-modal-ajanlat').on("submit",function(){
                    if($(this).valid()){
                        $dropzone.processQueue();
                        $dropzone.on("successmultiple", function(file,response) {
                            if(response == "1"){
                                $values = $('#form-modal-ajanlat').serialize();
                                $.post("/ajax/offer",$values,function(data){
                                    if(data.success)
                                        eval(data.javascript);
                                },"json");
                            }
                            return false;
                        });
                        return false;
                    }
            });
            $(".fancybox").fancybox();
            $('._ajax').on('submit',function(){
                $this = $(this);
                $this.find("_message").fadeOut();
                if($this.attr("data-loader") == "true"){
                    $this.find("._loader").fadeIn(300);
                }
                if($(this).data("chat"))
                    $(this).find("#new-message-input").val("");
                $values = $this.serialize();

                if($this.attr("data-disable") == "1"){
                    $this.find("input").attr("disabled","disabled");
                }
                $.post("/ajax/" + $(this).attr("data-action"),$values,function(data){
                    if(data.message != undefined){
                        if(data.html == true)
                            $this.find('._message').html(data.message);
                        else
                            $this.find('._message').text(data.message);
                    }
                    if(data.redirect_blank != undefined){
                          var win = window.open(data.redirect_blank, '_blank',"height=" + $(window).height() + "px,width="+($(window).width()-30)+"px");
                          win.focus();
                    }
                    if(data.reload != undefined)
                        top.location.reload();
                    if(data.redirect != undefined)
                        top.location.href = data.redirect;
                    if(data.javascript != undefined){
                        eval(data.javascript);
                    }
                    if($this.attr("data-loader") == "true"){
                        $this.find("._loader").fadeOut(300);
                    }
                    if($this.attr("data-redirect-uri").length > 0){
                        top.location.href = $this.attr("data-redirect-uri");
                    }
                },"json");
                return false;
            });
        },
        login: function() {
            var r = $("#form-login"),
                s = $("#form-reminder"),
                o = $("#form-register");
            $("#link-register-login").click(function() {
                e(r, o, "register")
            }), $("#link-register").click(function() {
                e(o, r, "")
            }), $("#link-reminder-login").click(function() {
                e(r, s, "reminder")
            }), $("#link-reminder").click(function() {
                e(s, r, "")
            }), "#register" === window.location.hash && (r.hide(), o.show()), "#reminder" === window.location.hash && (r.hide(), s.show()), 
            $('#form-login').on("submit",function(){
                    if($(this).valid()){
                        $values = $(this).serialize();
                        $.post("/ajax/login",$values,function(data){
                            if(data.status == "0"){
                                errors = { "login-password": "Helytelen jelszót adtál meg." };
                                $formlogin.showErrors(errors);  
                            }else{
                                if(data.redirect != undefined)
                                    top.location.href = data.redirect;
                            }
                        },"json");
                    }
                    return false;
            });
            $('#form-recovery-sent').on("submit",function(){
                    if($(this).valid()){
                        $values = $(this).serialize();
                        $.post("/ajax/recovery",$values,function(data){
                            var errors = [];
                            if(data.status == "0"){
                                if(data.code != undefined)
                                    $formregistersuccess.showErrors({"auth-code" : "Hibás kód!"});
                            }else{
                                $('#form-recovery-sent').slideUp(250);
                                $('#form-recovery-new-password').slideDown(250);
                                $('#form-recovery-new-password #recovery-token').val(data.token);
                                $('#form-recovery-new-password #recovery-email').val($('#reminder-email').val());
                            }
                        },"json");
                    }
                    return false;
            });
            $('#form-recovery-new-password').validate({
                errorClass: "help-block animation-slideDown",
                errorElement: "div",
                errorPlacement: function(e, r) {
                    r.parents(".form-group > div").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".form-group").removeClass("has-success has-error"), e.closest(".help-block").remove()
                },
                rules: {
                    "recovery-password": {
                        required: !0,
                        minlength: 5
                    },
                    "recovery-password-verify": {
                        required: !0,
                        equalTo: "#recovery-password"
                    }
                },
                messages: {
                    "recovery-password": {
                        required: "Kérlek adj meg egy jelszót.",
                        minlength: "A jelszónak minimum 5 karakterből kell, hogy álljon."
                    },
                    "recovery-password-verify": {
                        required: "Kérlek adj meg egy jelszót.",
                        minlength: "A jelszónak minimum 5 karakterből kell, hogy álljon.",
                        equalTo: "A két jelszónak meg kell egyezni."
                    },
                }
            });
            $("#form-recovery-sent").validate({
                errorClass: "help-block animation-slideDown",
                errorElement: "div",
                errorPlacement: function(e, r) {
                    r.parents(".form-group > div").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".form-group").removeClass("has-success has-error"), e.closest(".help-block").remove()
                },
                rules: {
                    "code-auth": {
                        required: 1,
                        minlength: 5
                    }
                },
                messages: {
                    "code-auth": "A kódot kötelező megadni."
                }
            });
            $('#form-reminder').on("submit",function(){
                    if($(this).valid()){
                        $values = $(this).serialize();
                        $.post("/ajax/recovery",$values,function(data){
                            if(data.status == "0"){
                                errors = { "reminder-email": "Helytelen email címet adtál meg." };
                                $formreminder.showErrors(errors);  
                            }else{
                                $('#form-reminder').slideUp(250);
                                $('#form-recovery-sent').slideDown(250);

                            }
                        },"json");
                    }
                    return false;
            });

            $('#form-recovery-new-password').on("submit",function(){
                    if($(this).valid()){
                        $values = $(this).serialize();
                        $.post("/ajax/recovery",$values,function(data){
                            if(data.status == "0"){
                                if(data.email != undefined)
                                    $formregister.showErrors({"recovery-password-verify" : "Ismeretlen hiba történt, kérlek próbáld újra!"});
                            }else{
                                $('#form-recovery-new-password').slideUp(250);
                                $('#form-recovery-success').slideDown(250);
                                setTimeout(function(){
                                    top.location.href = data.redirect;
                                },5000);
                            }
                        },"json");
                    }
                    return false;
            });
            $('#form-register').on("submit",function(){
                    if($(this).valid()){
                        $values = $(this).serialize();
                        $.post("/ajax/register",$values,function(data){
                            if(data.redirect != undefined)
                                top.location.href = data.redirect;
                            var errors = [];
                            if(data.status == "0"){
                                if(data.email != undefined)
                                    $formregister.showErrors({"register-email" : "Az email cím már regisztrálva van."});
                                if(data.phone != undefined)
                                    $formregister.showErrors({"register-phone" : "A telefonszám már regisztrálva van."});
                                if(data.username != undefined)
                                    $formregister.showErrors({"register-username" : "A felhasználónév már foglalt."});
                            }else{
                                $('#form-register').slideUp(250);
                                $('#form-register-success').slideDown(250);
                            }
                        },"json");
                    }
                    return false;
            });
            $('#form-register-success').on("submit",function(){
                    if($(this).valid()){
                        $values = $(this).serialize();
                        $.post("/ajax/verification",$values,function(data){
                            var errors = [];
                            if(data.status == "0"){
                                if(data.code != undefined)
                                    $formregistersuccess.showErrors({"auth-code" : "Hibás kód!"});
                            }else{
                                $('#form-register-success').slideUp(250);
                                $('#form-auth-success').slideDown(250);
                                setTimeout(function(){
                                    top.location.href = data.redirect;
                                },3000);
                            }
                        },"json");
                    }
                    return false;
            });
            $formregistersuccess = $("#form-register-success").validate({
                errorClass: "help-block animation-slideDown",
                errorElement: "div",
                errorPlacement: function(e, r) {
                    r.parents(".form-group > div").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".form-group").removeClass("has-success has-error"), e.closest(".help-block").remove()
                },
                rules: {
                    "code-auth": {
                        required: 1,
                        minlength: 5
                    }
                },
                messages: {
                    "code-auth": "A kódot kötelező megadni."
                }
            }), 
            $formlogin = $("#form-login").validate({
                errorClass: "help-block animation-slideDown",
                errorElement: "div",
                errorPlacement: function(e, r) {
                    r.parents(".form-group > div").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".form-group").removeClass("has-success has-error"), e.closest(".help-block").remove()
                },
                rules: {
                    "login-email": {
                        required: 1
                    },
                    "login-password": {
                        required: 1,
                        minlength: 5
                    }
                },
                messages: {
                    "login-email": "Kérlek add meg az email címed vagy a felhasználó neved.",
                    "login-password": {
                        required: "Kérlek add meg a jelszavad.",
                        minlength: "A jelszavad minimum 5 karakterből kell, hogy álljon."
                    }
                }
            }), 
            $formreminder = $("#form-reminder").validate({
                errorClass: "help-block animation-slideDown",
                errorElement: "div",
                errorPlacement: function(e, r) {
                    r.parents(".form-group > div").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".form-group").removeClass("has-success has-error"), e.closest(".help-block").remove()
                },
                rules: {
                    "reminder-email": {
                        required: !0,
                        email: !0
                    }
                },
                messages: {
                    "reminder-email": "Kérlek add meg az email címed."
                }
            }), 
            $formregister = $("#form-register").validate({
                errorClass: "help-block animation-slideDown",
                errorElement: "div",
                errorPlacement: function(e, r) {
                    r.parents(".form-group > div").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    2 === e.closest(".form-group").find(".help-block").length ? e.closest(".help-block").remove() : (e.closest(".form-group").removeClass("has-success has-error"), e.closest(".help-block").remove())
                },
                rules: {
                    "register-firstname": {
                        required: !0,
                        minlength: 2
                    },
                    "register-lastname": {
                        required: !0,
                        minlength: 2
                    },
                    "register-username": {
                        required: !0,
                        minlength: 2
                    },
                    "register-phone": {
                        required: !0,
                        minlength: 2
                    },
                    "register-email": {
                        required: !0,
                        email: !0
                    },
                    "register-password": {
                        required: !0,
                        minlength: 5
                    },
                    "register-password-verify": {
                        required: !0,
                        equalTo: "#register-password"
                    },
                    "register-terms": {
                        required: !0
                    }
                },
                messages: {
                    "register-firstname": {
                        required: "Kérlek add meg a vezetéknevedet.",
                        minlength: "Kérlek add meg a vezetéknevedet."
                    },
                    "register-lastname": {
                        required: "Kérlek add meg a keresztnevedet.",
                        minlength: "Kérlek add meg a keresztnevedet."
                    },
                    "register-username": {
                        required: "Kérlek add meg a Felhasználó nevedet.",
                        minlength: "Kérlek add meg a Felhasználó nevedet."
                    },
                    "register-phone": {
                        required: "Kérlek add meg a telefonszámodat.",
                        minlength: "Kérlek add meg a telefonszámodat."
                    },
                    "register-email": "Kérlek adj meg egy érvényes email címet.",
                    "register-password": {
                        required: "Kérlek adj meg egy jelszót.",
                        minlength: "A jelszónak minimum 5 karakterből kell, hogy álljon."
                    },
                    "register-password-verify": {
                        required: "Kérlek adj meg egy jelszót.",
                        minlength: "A jelszónak minimum 5 karakterből kell, hogy álljon.",
                        equalTo: "A két jelszónak meg kell egyezni."
                    },
                    "register-terms": {
                        required: "Kérlek fogadd el a felhasználási feltételeket."
                    }
                }
            })
        }
    }
}();