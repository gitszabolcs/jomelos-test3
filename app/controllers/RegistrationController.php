<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



class RegistrationController extends BaseController {
    
    public function registrate(){
        $validator = Validator::make(Input::all(),array(
                                                        'username' => 'required',
                                                        'password' => 'required',
                                                        'first_name' => 'required',
                                                        'last_name' => 'required',
                                                        'email' => 'required',
                                                        'phone' => 'required'
                                                        ));
        
        if($validator->fails()){
            return Redirect::route('signup#register')
								->withErrors($validator)
								->withInput();
        }
    }



}