<?php

class MainController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	

	protected $layout = 'layouts.main';

	public function fooldal()
	{
		$this->layout->content = View::make('blade.index');
	}
         * 
         */
    public function fooldal()
    {
            $jobs[] = array('name'=>'1','title'=>'1', 'location'=>'1');
            $jobs[] = array('name'=>'2','title'=>'2', 'location'=>'2');
            $jobs[] = array('name'=>'3','title'=>'3', 'location'=>'3');
            $data['jobs'] = $jobs;
            return View::make('blade.index',$data);
    }
    public function szakemberek()
    {
            return View::make('blade.szakemberek');
    }
}