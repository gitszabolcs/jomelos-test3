<?php

class AuthentificationController extends BaseController {

	public function getSignUp(){
		return View::make('account.sign-up');
	}

	public function getSignIn(){
        return View::make('account.sign_in');
	}
        
    public function postSignIn() {
        $validator = Validator::make(Input::all(),array(
                                                        'username' => 'required',
                                                        'password' => 'required'
                                                        )

								);
        
		if($validator->fails()){
			return Redirect::route('get-sign-in')
								->withErrors($validator)
								->withInput();
		}else{

			$auth = Auth::attempt(array(
									'username' => Input::get('username'),
									'password' => Input::get('password')
				),true);
		}

		if($auth){
			return Redirect::intended('/');
		}else{
			return Redirect::route('get-sign-in')
							->with('global', 'Username/password wrong..');
		}

		return Redirect::route('get-sign-in')
							->with('global', 'There was a problem signing you in.');
        }

}