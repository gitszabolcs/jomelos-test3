<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
| /kereses/@keresettKategoria/@keresettAlKategoria {POST|GET}
*/

Route::get('/signup',array(
                        'uses'  => 'AuthentificationController@getSignUp',
                        'as'    => 'get-sign-up'
));

Route::post('/signup',array(
                        'uses'  => 'AuthentificationController@postSignUp',
                        'as'    => 'account-sign-up-post'
));

Route::get('/signin',array(
                        'uses'  => 'AuthentificationController@getSignIn',
                        'as'    => 'get-sign-in'
));

Route::post('/signin',array(
                        'uses'  => 'AuthentificationController@postSignIn',
                        'as'    => 'account-sign-in-post'
));

Route::get('/createsuperadmin',function(){
    User::create(array(
                    'username'  => 'superadmin',
                    'password'  => Hash::make('123qwe'),
                    'title'     => 'superadmin'
    ));
});

Route::get('/', 'MainController@fooldal');

Route::get('/szakemberek','SzakemberekController@index');

Route::get('/contact', function(){
    echo 'Contact page';
});

Route::get('/munka/{id}/{nev}/{action?}', function($id, $nev, $action = null){
	
	/*
	| ---------------------------------------------
	| Path parameters
	| ---------------------------------------------
	|
	|	@id     -> kotelezo & egyedi
	|			-> int
	|
	|	@nev    -> kotelezo
	|			-> id alapjan
	|			-> ha nincs: adatbazis hiba
	|
	|	@action -> nem kotelezo
	|			-> munkaval kapcsolatos muveletek
	|
	|
	| ---------------------------------------------
	| Path test
	| ---------------------------------------------
	|
	| URL: 
	|		/munka/1/favagas/torol
	|
	|		/munka/2/favagas
	| 
	| echo 'munka<br />id: '.$id.'<br />nev: '.$nev.'<br />muvelet: '.$action;
	*/
	

}) -> where('id', '[0-9]+');

Route::get('/bejelentkezes', function(){

	/*
	| ---------------------------------------------
	| Login Page
	| ---------------------------------------------
	*/

    echo 'Login page {GET}';
});

Route::post('/bejelentkezes', function(){

	/*
	| ---------------------------------------------
	| Login PostProcess
	| ---------------------------------------------
	*/
	
    $validLogin = true;
    
    if($validLogin) 
    {
    	Session::put('loggedInUser', 'userid');

    	return Redirect::to('/');

    } else 
    	{

    		Session::put('loggedInUser', 0);

    		return Redirect::to('/bejelentkezes') -> with('message', 'No login for you mister!');

    	}

});

Route::get('/munkak', function(){

	/*
	| ---------------------------------------------
	| Munkak Page
	| ---------------------------------------------
	*/
	
});



Route::get('/felhasznalo/{nev}/{action?}', function($nev, $action = null){
	
	/*
	| ---------------------------------------------
	| Path parameters
	| ---------------------------------------------
	|
	|	@nev    -> kotelezo
	|			-> ha nincs: adatbazis hiba
	|
	|	@action -> nem kotelezo
	|			-> munkaval kapcsolatos muveletek
	|
	|
	| ---------------------------------------------
	| Path test
	| ---------------------------------------------
	|
	| URL: 
	|		/felhasznalo/jani/torol
	|
	|		/felhasznalo/jani
	| 
	| echo 'felhasznalo<br />nev: '.$nev.'<br />muvelet: '.$action;
	*/
});