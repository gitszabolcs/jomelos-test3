<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Jomelós</title>
    {{ HTML::style( URL::asset('css/bootstrap.min.css') ) ; }}
    {{ HTML::style( URL::asset('css/font-awesome-4-1-0.css') ) ; }}
	@yield('style')
</head>
<body>
	<div>
        @include('includes.header')
    </div>
     
    <div>
        @yield("content")
    </div>


    @section('footer')
    	@include('includes.footer')
    @show

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    {{ HTML::script( URL::asset('js/bootstrap.min.js') ) ; }}
    {{ HTML::script( URL::asset('js/tab.js') ) ; }}
    {{ HTML::script( URL::asset('js/jquery.bxslider.js') ) ; }}
    @yield('scripts')
</body>
</html>