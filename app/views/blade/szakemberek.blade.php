@extends('layouts.main')

@section('style')
	{{ HTML::style( URL::asset('css/bootstrap.min.css') ) ; }}
	{{ HTML::style( URL::asset('css/font-awesome-4-1-0.css') ) ; }}
	{{ HTML::style( URL::asset('css/plugins.css') ) ; }}
	{{ HTML::style( URL::asset('css/themes.css') ) ; }}
	{{ HTML::style( URL::asset('css/jquery.bxslider.css') ) ; }}
	{{ HTML::style( URL::asset('css/custom.css') ) ; }}
@stop

@section("content")
     <!-- szakember slider -->
    <div class="container">
      <div class="row">
        <div class="label-holder" style="padding-left:5px;">
          <div class="label label-default label-info label-blue label-big">
            <i class="fa fa-group"></i>
            Szakemberek
          </div>
          <div class="cs-w-slider-title-left">
            <a href="#">
              <small>Szeretne itt megjelenni?</small>
            </a>
          </div>
        </div>
        
        <ul class="slider2">

          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
        </ul>
        <div class="bx_customNavigation">
          <div id="slider-bx-prev2" class="btn prev"></div>
          <div id="slider-bx-next2" class="btn next"></div>
        </div>
      </div><!-- /row -->
    </div><!-- /container -->
    <!-- /szakember slider -->
    
    <!-- szakemberek head kereső --> 
    <div class="container">
            
            <div class="row munkak-kereso-box szakember-kereso-box my-center" style="max-width: 1110px">
          
              <form class=" form-inline" method="post" action="#">
          
                <div class="padding-fix-searcher szakemberek-kereso-blocker szakemberek-kereso-blocker-longer">
                  <div class="col-xs-12 padding-bottom-10"> Keresés szakember nevére? </div>
                  <div class="col-xs-12">
                    <select id="example-chosen-multiple" name="szakember_tags" class="select-chosen" data-placeholder="Kezdjen el gépelni..." multiple>
                      <option value="">Teszt Elek</option>
                      <option value="">Whinch Eszter</option>
                      <option value="">Péter Pál</option>
                      <option value="">Kovács Kettő Tizedes</option>
                    </select>
                  </div>
                </div>
                
                <div class="padding-fix-searcher szakemberek-kereso-blocker szakemberek-kereso-blocker-longer">
                  <div class="col-xs-12 padding-bottom-10">Milyen munkára van szüksége? </div>
                  <div class="col-xs-12">
                    <select id="example-chosen" name="munkak_tags" class="select-chosen" data-placeholder="Kezdjen el gépelni..." multiple>
                      <option value="">Ilyen</option>
                      <option value="">Olyan</option>
                      <option value="">Amolyan</option>
                      <option value="">Emilyen</option>
                    </select>
                  </div>
                </div>
                
                <div class="padding-fix-searcher szakemberek-kereso-blocker szakemberek-kereso-blocker-normal">
                  <div class="col-xs-12 padding-bottom-10">Hol? </div>
                  <div class="col-xs-12">
                    <select id="example-chosen" name="munkak_tags" class="select-chosen" data-placeholder="Hol?">
                      <option value="">Itt</option>
                      <option value="">Ott</option>
                      <option value="">Amott</option>
                    </select>
                  </div>
                </div>

                <div class="szakemberek-kereso-blocker szakemberek-kereso-blocker-normal">
                  <div class="slider_one">
                    <span class="col-xs-12">Szakember értékelése</span>
                    <div class="col-xs-12">
                      <input name="szakember_rate" id="ex1" type="text" class="span2" value="" data-slider-min="1" data-slider-max="5" data-slider-step="1" data-slider-value="[2,3]" />
                    </div>
                    <div class="col-xs-12 searchbox-gray-2 search-box-mini-titles munkak-searcher-box-slider-titles">
                      <div class="col-xs-6 text-left munkak-searcher-box-text-1"><small>1</small></div>
                      <div class="col-xs-6 text-right munkak-searcher-box-text-2"><small>5</small></div>
                    </div>
                  </div>
                </div>
                
                <div class="szakemberek-kereso-blocker szakemberek-kereso-blocker-normal">
                  <div class="slider_one">
                    <span class="col-xs-12">Órabér</span>
                    <div class="col-xs-12">
                      <input name="search_s_price" id="ex2" type="text" class="span2" value="" data-slider-min="100" data-slider-max="100000" data-slider-step="100" data-slider-value="[12500,45000]" />
                    </div>
                    <div class="col-xs-12 searchbox-gray-2 search-box-mini-titles munkak-searcher-box-slider-titles">
                      <div class="col-xs-6 text-left munkak-searcher-box-text-1"><small>100 Ft</small></div>
                      <div class="col-xs-6 text-right munkak-searcher-box-text-2"><small>100.000 Ft</small></div>
                    </div>
                  </div>
                </div>
                <div class="szakemberek-kereso-blocker szakemberek-kereso-blocker-shorter">
                  <div class="munkak-kereso-btn-box">
                    <button type="button" class="btn btn-default searcher-box-button jumbo-btn-shadows">Keresés</button>
                  </div>
                </div>
              </form>
              <!-- -->
              
            </div>
            <!-- /munkak-kereso-box end -->
            
          </div><!-- /container -->
    <!-- /szakemberek head kereső -->
    
    
   
    <!-- szakemberek lista -->
    <div class="container">
      
        <!--
        
            TODO
                
                A table STRIPE (odd||even) csíkozásával nem kell
                foglalkozni, egy JS script megcsinálja magától!!!!!
        
            Megtekintés gomb: form-ra kötve!
        
        -->
        
    <div class="mr-block-right mr-block-holder my-center" style="max-width: 1110px">
        <div class="munka-reszletek-header munka-reszletek-block-head-bg">
            Szakemberek
        </div>
        
        <div class="table-responsive tab-content-mini">
          <table class="table table-vcenter table-borderless" id="a-munka-tabla">
            <thead>
              <tr class="white">
                <th class="text-center szakember-table-picrow" style="width:79px;"><i class="fa fa-picture-o"></i></th>
                <th>Személyes adatok</th>
                <th>Szakterület</th>
                <th class="text-center">Munkát vállal</th>
                <th class="text-center">Órabér</th>
                <th style="width: 109px;"><!-- spacer --></th>
              </tr>
            </thead>
            
            <tbody>
              
              <!-- item -->
              <tr class=" munkak-table-datarow">
                <td class="text-center">
                    <div class="szakember-table-pic-pic">
                        <img src="http://lorempixel.com/65/65/people" alt="avatar" class="munkak-table-img" />
                    </div>
                    <div class="szakember-table-pic-btn">
                        <form action="#" method="get">
                            <button type="button" class="btn btn-default btn-xs szakember-table-pic-btn-btn"><i class="fa fa-envelope"></i> Kapcsolat</button>
                        </form>
                    </div>
                </td>
                <td>
                    <div class="szakember-lista-pd-nev">
                        <a href="#">Zoliapu</a>
                    </div>
                    <div class="szakember-lista-pd-starrate">
                        
                            <span class="glyphicon glyphicon-star  szakember-rate-color-good"></span>
                            <span class="glyphicon glyphicon-star  szakember-rate-color-good"></span>
                            <span class="glyphicon glyphicon-star  szakember-rate-color-good"></span>
                            <span class="glyphicon glyphicon-star  szakember-rate-color-good"></span>
                            <span class="glyphicon glyphicon-star  szakember-rate-color-bad"></span>
                            <span class="slider-item-rate-number"> 4.9</span>
                        
                    </div>
                    <div class="szakember-lista-pd-ratebtns">
                        <div class="m-slider-wrapper">
                            <form action="#" method="" class="noform">
                                <input type="hidden" name="szakember_rate_id" value="" />
                                <input type="hidden" name="szakember_rate_type" value="1" />
                                <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                            </form>
                            <span class="szakember-rate-txt"><img src="img/pozitiv.png"  style="width: 20px;"/><small>65</small></span>
                        
                            <form action="#" method="" class="noform">
                                <input type="hidden" name="szakember_rate_id" value="" />
                                <input type="hidden" name="szakember_rate_type" value="2" />
                                <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                            </form>
                            <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
                            
                            <form action="#" method="" class="noform">
                                <input type="hidden" name="szakember_rate_id" value="" />
                                <input type="hidden" name="szakember_rate_type" value="3" />
                                <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                            </form>
                            <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                        </div><!-- /m-slider-wrapper -->
                    </div>
                    <div class="szakember-lista-pd-reviews">
                        <small>(43 vélemény)</small>
                    </div>
                </td>
                <td >Kőműves, tetőfedő, ács</td>
                <td class="text-center ">Somogymegye</td>
                <td class="text-center ">1000 Ft/óra</td>
                <td class="text-center">
                    <div class="szakember-lista-opt-holder">
                        <div class="szakember-lista-opt-up">
                            <div class="label label-kiemelt">Kiemelt</div>
                        </div>
                        <div class="szakember-lista-opt-dn">
                          <div class="szakember-lista-opt-dn-btn">
                            <form action="" methot="get">
                              <button type="button" class="btn btn-default searcher-box-button jumbo-btn-shadows"><i class="hi hi-eye-open"></i> Megtekint</button>
                            </form>
                          </div>
                        </div>
                    </div>
                </td>
              </tr>
              <tr class="munkak-table-hovertext">
                <td></td>
                <td colspan="5">
                    <div class="szakember-table-hovertext-placer">
                        <div class="szakember-table-hovertext-head">
                            Bemutatkozás:
                        </div>
                        <div class="szakember-table-hovertext-txt">
                            Etiam varius justo lacus, non commodo nisl elementum non. Fusce quis turpis non mauris aliquet varius.
                            Mauris porta rutrum commodo. Nullam mattis condimentum justo, sit amet pellentesque mauris. Nunc quis quam
                            vitae est blandit gravida. Aenean fringilla ornare tellus ac tempus. Suspendisse viverra hendrerit dolor.
                            Aenean viverra luctus aliquet. Mauris non convallis lorem. Duis molestie et urna ut viverra.
                            Nam in aliquet urna. Nunc ac massa at enim placerat auctor at eu orci.
                        </div>
                    </div>
                </td>
              </tr>
              <!-- /item -->
              
              
              
<!-- item -->
              <tr class=" munkak-table-datarow">
                <td class="text-center">
                    <div class="szakember-table-pic-pic">
                        <img src="http://lorempixel.com/65/65/people" alt="avatar" class="munkak-table-img" />
                    </div>
                    <div class="szakember-table-pic-btn">
                        <form action="#" method="get">
                            <button type="button" class="btn btn-default btn-xs szakember-table-pic-btn-btn"><i class="fa fa-envelope"></i> Kapcsolat</button>
                        </form>
                    </div>
                </td>
                <td>
                    <div class="szakember-lista-pd-nev">
                        <a href="#">Zoliapu</a>
                    </div>
                    <div class="szakember-lista-pd-starrate">
                       <span class="glyphicon glyphicon-star  szakember-rate-color-good"></span>
                            <span class="glyphicon glyphicon-star  szakember-rate-color-good"></span>
                            <span class="glyphicon glyphicon-star  szakember-rate-color-good"></span>
                            <span class="glyphicon glyphicon-star  szakember-rate-color-good"></span>
                            <span class="glyphicon glyphicon-star  szakember-rate-color-bad"></span>
                            <span class=" slider-item-rate-number"> 4.9</span>
                    </div>
                    <div class="szakember-lista-pd-ratebtns">
                        <div class="m-slider-wrapper">
                            <form action="#" method="" class="noform">
                                <input type="hidden" name="szakember_rate_id" value="" />
                                <input type="hidden" name="szakember_rate_type" value="1" />
                                <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                            </form>
                            <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
                        
                            <form action="#" method="" class="noform">
                                <input type="hidden" name="szakember_rate_id" value="" />
                                <input type="hidden" name="szakember_rate_type" value="2" />
                                <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                            </form>
                            <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
                            
                            <form action="#" method="" class="noform">
                                <input type="hidden" name="szakember_rate_id" value="" />
                                <input type="hidden" name="szakember_rate_type" value="3" />
                                <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                            </form>
                            <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                        </div><!-- /m-slider-wrapper -->
                    </div>
                    <div class="szakember-lista-pd-reviews">
                        <small>(43 vélemény)</small>
                    </div>
                </td>
                 <td class="">Kőműves, tetőfedő, ács</td>
                <td class="text-center ">Somogymegye</td>
                <td class="text-center ">1000 Ft/óra</td>
                <td class="text-center ">
                    <div class="szakember-lista-opt-holder">
                        <div class="szakember-lista-opt-up">
                            <div class="label label-kiemelt">Kiemelt</div>
                        </div>
                        <div class="szakember-lista-opt-dn">
                          <div class="szakember-lista-opt-dn-btn">
                            <form action="" methot="get">
                              <button type="button" class="btn btn-default searcher-box-button jumbo-btn-shadows"><i class="hi hi-eye-open"></i> Megtekint</button>
                            </form>
                          </div>
                        </div>
                    </div>
                </td>
              </tr>
              <tr class="munkak-table-hovertext ">
                <td></td>
                <td colspan="5">
                    <div class="szakember-table-hovertext-placer">
                        <div class="szakember-table-hovertext-head">
                            Bemutatkozás:
                        </div>
                        <div class="szakember-table-hovertext-txt">
                            Etiam varius justo lacus, non commodo nisl elementum non. Fusce quis turpis non mauris aliquet varius.
                            Mauris porta rutrum commodo. Nullam mattis condimentum justo, sit amet pellentesque mauris. Nunc quis quam
                            vitae est blandit gravida. Aenean fringilla ornare tellus ac tempus. Suspendisse viverra hendrerit dolor.
                            Aenean viverra luctus aliquet. Mauris non convallis lorem. Duis molestie et urna ut viverra.
                            Nam in aliquet urna. Nunc ac massa at enim placerat auctor at eu orci.
                        </div>
                    </div>
                </td>
              </tr>
              <!-- /item -->
                
<!-- item -->
              <tr class=" munkak-table-datarow">
                <td class="text-center">
                    <div class="szakember-table-pic-pic">
                        <img src="http://lorempixel.com/65/65/abstract" alt="avatar" class="munkak-table-img" />
                    </div>
                    <div class="szakember-table-pic-btn">
                        <form action="#" method="get">
                            <button type="button" class="btn btn-default btn-xs szakember-table-pic-btn-btn"><i class="fa fa-envelope"></i> Kapcsolat</button>
                        </form>
                    </div>
                </td>
                <td>
                    <div class="szakember-lista-pd-nev">
                        <a href="#">Zoliapu</a>
                    </div>
                    <div class="szakember-lista-pd-starrate">
                        <span class="glyphicon glyphicon-star  szakember-rate-color-good"></span>
                            <span class="glyphicon glyphicon-star  szakember-rate-color-good"></span>
                            <span class="glyphicon glyphicon-star  szakember-rate-color-good"></span>
                            <span class="glyphicon glyphicon-star  szakember-rate-color-good"></span>
                            <span class="glyphicon glyphicon-star  szakember-rate-color-bad"></span>
                            <span class=" slider-item-rate-number"> 4.9</span>
                    </div>
                    <div class="szakember-lista-pd-ratebtns">
                        <div class="m-slider-wrapper">
                            <form action="#" method="" class="noform">
                                <input type="hidden" name="szakember_rate_id" value="" />
                                <input type="hidden" name="szakember_rate_type" value="1" />
                                <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                            </form>
                            <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
                        
                            <form action="#" method="" class="noform">
                                <input type="hidden" name="szakember_rate_id" value="" />
                                <input type="hidden" name="szakember_rate_type" value="2" />
                                <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                            </form>
                            <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
                            
                            <form action="#" method="" class="noform">
                                <input type="hidden" name="szakember_rate_id" value="" />
                                <input type="hidden" name="szakember_rate_type" value="3" />
                                <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                            </form>
                            <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                        </div><!-- /m-slider-wrapper -->
                    </div>
                    <div class="szakember-lista-pd-reviews">
                        <small>(43 vélemény)</small>
                    </div>
                </td>
                <td class="">Kőműves, tetőfedő, ács</td>
                <td class="text-center ">Somogymegye</td>
                <td class="text-center ">1000 Ft/óra</td>
                <td class="text-center">
                    <div class="szakember-lista-opt-holder">
                        <div class="szakember-lista-opt-up">
                            <div class="label label-kiemelt">Kiemelt</div>
                        </div>
                        <div class="szakember-lista-opt-dn">
                          <div class="szakember-lista-opt-dn-btn">
                            <form action="" methot="get">
                              <button type="button" class="btn btn-default searcher-box-button jumbo-btn-shadows"><i class="hi hi-eye-open"></i> Megtekint</button>
                            </form>
                          </div>
                        </div>
                    </div>
                </td>
              </tr>
              <tr class="munkak-table-hovertext ">
                <td></td>
                <td colspan="5">
                    <div class="szakember-table-hovertext-placer">
                        <div class="szakember-table-hovertext-head">
                            Bemutatkozás:
                        </div>
                        <div class="szakember-table-hovertext-txt">
                            Etiam varius justo lacus, non commodo nisl elementum non. Fusce quis turpis non mauris aliquet varius.
                            Mauris porta rutrum commodo. Nullam mattis condimentum justo, sit amet pellentesque mauris. Nunc quis quam
                            vitae est blandit gravida. Aenean fringilla ornare tellus ac tempus. Suspendisse viverra hendrerit dolor.
                            Aenean viverra luctus aliquet. Mauris non convallis lorem. Duis molestie et urna ut viverra.
                            Nam in aliquet urna. Nunc ac massa at enim placerat auctor at eu orci.
                        </div>
                    </div>
                </td>
              </tr>
              <!-- /item -->
            </tbody>
          </table>
          </div>
        </div><!-- /resp table div end -->
        <!-- -->
    
    
        <!-- lapozo -->
        <div class="text-center">
          <ul class="pagination pagination-rounded">
            <li><a href="javascript:void(0)">&laquo;</a></li>
            <li class="active"><a href="javascript:void(0)">1</a></li>
            <li><a href="javascript:void(0)">2</a></li>
            <li><a href="javascript:void(0)">3</a></li>
            <li><a href="javascript:void(0)">4</a></li>
            <li><a href="javascript:void(0)">&raquo;</a></li>
          </ul>
        </div>
        <!-- /lapozo -->
        
     </div>
    </div>
    <!-- /szakemberek lista -->
   
   
    

    <!-- footer -->
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 footer-dark-spacer">
          
        </div>
      </div>
    </div>
@stop

@section('scripts')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	{{ HTML::script( URL::asset('js/bootstrap.min.js') ) ; }}
	{{ HTML::script( URL::asset('js/tab.js') ) ; }}
	{{ HTML::script( URL::asset('js/jquery.bxslider.js') ) ; }}
	{{ HTML::script( URL::asset('js/custom.js') ) ; }}
	{{ HTML::script( URL::asset('js/app2.js') ) ; }}
	{{ HTML::script( URL::asset('js/plugins2.js') ) ; }}
	{{ HTML::script( URL::asset('js/bootstrap-slider.js') ) ; }}
	<script>
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&appId=362347407113313&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>

@stop