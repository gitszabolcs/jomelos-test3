@extends('layouts.main')
<!-- saddssad-->



@section('style')
	{{ HTML::style( URL::asset('css/bootstrap.min.css') ) ; }}
	{{ HTML::style( URL::asset('css/font-awesome-4-1-0.css') ) ; }}
	{{ HTML::style( URL::asset('css/plugins.css') ) ; }}
	{{ HTML::style( URL::asset('css/themes.css') ) ; }}
	{{ HTML::style( URL::asset('css/jquery.bxslider.css') ) ; }}
	{{ HTML::style( URL::asset('css/custom.css') ) ; }}
@stop

@section("content")
	<!-- JUMBOTRON HEADING IMAGE -->
    <div id="jumbotron-no-margin" class="jumbotron hidden-xs hidden-sm">
      <div class="container">

        <div class="row">
          <div class="col-sm-12 text-center txt-keresek-hoak">
            <img src="img/txt_keresek.png" alt="" title="" class="animation-fadeInQuick" />
          </div>
        </div>

        <div class="row jumbo-row">
          <div class="col-lg-6 col-md-6 col-sm-6 text-right jumbo-leftside" id="jumbo-left-place">
            <div class="row">

              <div class="col-md-6 hidden-sm jumbo-image-holder image-valign-bottom">
                <img src="img/family.png" alt="" title="" />
              </div>

              <div class="col-md-6">
                <div class="row jumbo_spacer_top"></div>
                <div class="row jumbo_image_spacer text-center">
                  <img src="img/txt_jomunkat.png" alt="" title="" />
                </div>
                <div class="row text-center">
                  <form action="#" method="get">
                    <button type="button" class="btn_jumbo_munka">Munkák megtekintése</button>
                  </form>
                </div>
                <div class="row jumbo_spacer_bott"></div>
              </div>
              
            </div>
          </div>

          <div class="col-lg-6 col-md-6 text-left jumbo-rightside" id="jumbo-right-place">
            <div class="row">
              
              <div class="col-md-6 hidden-sm jumbo_kep_jobb image-valign-bottom jumbo-image-holder">
                <img src="img/worker.png" alt="" title="" />
              </div>

              <div class="col-md-6">
                <div class="row jumbo_spacer_top"></div>
                <div class="row jumbo_image_spacer text-center">
                  <img src="img/txt_jomelost.png" alt="" title="" />
                </div>
                <div class="row text-center">
                  <form action="#" method="get">
                    <button type="button" class="btn_jumbo_melos">Munka beküldése</button>
                  </form>
                </div>
                <div class="row jumbo_spacer_bott"></div>
              </div>

            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-sm-12 text-center jumbo-center-link">
            &nbsp; <a id="jumbo-how-to-work" href="#">Hogyan működik?</a> &nbsp; 
          </div>
        </div>
      </div>
      <div class="jumbotron-jomelos"></div>
    </div>
    <!-- jumbotron end -->

    <!-- searcher line -->
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-lg-9">
          <div class="searchbox">

            <div class="block full">
              <!-- Block Tabs Title -->
              <div class="block-title">
                <ul data-toggle="tabs" class="nav nav-tabs jomunkas-searcher-box" id="myTab">
                  <li class="tab-default-color active"><a href="#example-tabs2-activity">Szakember keresése</a></li>
                  <li class="tab-default-color "><a href="#example-tabs2-profile">Munkák keresése</a></li>
                  <li class="pull-right disabled searcher-box-disabled hidden-xs">
                    <i>Kereső</i>
                    <div class="badge label-white-inverse"><span class="fa fa-search"></span></div>
                  </li>
                </ul>
              </div>
              <!-- END Block Tabs Title -->
            
            <!-- Tabs Content -->
            <div class="tab-content">
              <div id="example-tabs2-activity" class="tab-pane active">

                <form action="#" method="post">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="col-sm-12 searchpanel-lines">
                        <input name="search_s_nev" class="form-control input-sm" type="text" placeholder="Keresés felhasználónévre?" id="searchpanel_ppl" />
                      </div>
                      <div class="col-sm-12 searchpanel-lines">
                        <select name="search_s_tipus" class="form-control searcher-select" data-placeholder="Milyen fajta munka?">
                          <option value="">Milyen fajta munka?</option>
                        </select>
                        
                      </div>
                      <div class="col-sm-12 searchpanel-lines">
                        <select name="search_s_hol" class="form-control searcher-select" data-placeholder="Hol?">
                          <option value="">Hol?</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="slider_one">
                        <span class="col-xs-12">Órabér</span>
                        <div class="col-xs-12">
                          <input name="search_s_price" id="ex2" type="text" class="span2" value="" data-slider-min="100" data-slider-max="100000" data-slider-step="100" data-slider-value="[12500,45000]" />
                        </div>
                        <div class="col-xs-12 searchbox-gray-2 search-box-mini-titles">
                          <div class="col-xs-6 text-left"><small>100 Ft</small></div>
                          <div class="col-xs-6 text-right"><small>100.000 Ft</small></div>
                        </div>
                      </div>
                      
                      <div class="slider_two">
                        <span class="col-xs-12">Szakember értékelése</span>
                        <div class="col-xs-12">
                          <input name="search_s_worker" id="ex1" type="text" class="span2" value="" data-slider-min="1" data-slider-max="5" data-slider-step="1" data-slider-value="[2,3]" />
                        </div>
                        <div class="col-xs-12 searchbox-gray-2 search-box-mini-titles">
                          <div class="col-xs-6 text-left"><small>1</small></div>
                          <div class="col-xs-6 text-right"><small>5</small></div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-sm-12 text-center searchpanel-block">
                      <button type="button" class="btn btn-default searcher-box-button jumbo-btn-shadows" id="submit">
                        <span class="glyphicon glyphicon-search"></span>
                        Keresés
                        <span id="searchpanel_talalatok hidden-xs">
                          (Találatok: 12)
                        </span>
                      </button>
                    </div>
                  </div>
                </form>


              </div>
              <div id="example-tabs2-profile" class="tab-pane">
                <form action="#" method="post">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="col-sm-12 searchpanel-lines">
                        <input name="search_m_nev" class="form-control input-sm" type="text" placeholder="Keresés munkára?" id="searchpanel_ppl" />
                      </div>
                      <div class="col-sm-12 searchpanel-lines">
                        <select name="search_m_tipus" class="form-control searcher-select" data-placeholder="Keresés munka típusra?">
                          <option value="">
                            Keresés munka típusra?
                          </option>
                        </select>
                      </div>
                      <div class="col-sm-12 searchpanel-lines">
                        <select name="search_m_hol" class="form-control searcher-select" data-placeholder="Hol?">
                          <option value="0">Hol?</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="slider_one">
                        <span class="col-xs-12">Órabér</span>
                        <div class="col-xs-12">
                          <input name="search_m_price" id="ex3" type="text" class="span2" value="" data-slider-min="100" data-slider-max="100000" data-slider-step="100" data-slider-value="[12500,45000]" />
                        </div>
                        <div class="col-xs-12 searchbox-gray-2 search-box-mini-titles">
                          <div class="col-xs-6 text-left"><small>100 Ft</small></div>
                          <div class="col-xs-6 text-right"><small>100.000 Ft</small></div>
                        </div>
                      </div>
                      
                      <div class="slider_two">
                        <span class="col-xs-12">Össz érték</span>
                        <div class="col-xs-12">
                          <input name="search_m_fullprice" id="ex4" type="text" class="span2" value="" data-slider-min="100" data-slider-max="100000" data-slider-step="500" data-slider-value="[5000,30000]" />
                        </div>
                        <div class="col-xs-12 searchbox-gray-2 search-box-mini-titles">
                          <div class="col-xs-6 text-left"><small>100 Ft</small></div>
                          <div class="col-xs-6 text-right"><small>100.000 Ft</small></div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-sm-12 text-center searchpanel-block">
                      <button type="button" class="btn btn-default searcher-box-button jumbo-btn-shadows" id="submit">
                        <span class="glyphicon glyphicon-search"></span>
                        Keresés
                        <span id="searchpanel_talalatok hidden-xs">
                          (Találatok: 8)
                        </span>
                      </button>
                    </div>
                  </div>
                </form>

              </div>
              
            </div>
            <!-- END Tabs Content -->
          </div>

            
          </div>
        </div>
        <div class="col-md-4 col-lg-3">
          <!-- fb like box spacer for little displays -->
          <div class="fb-box-spacer visible-xs hidden-sm hidden-md hidden-lg"></div>
          
          <!-- FB like box -->
          <div class="fb_box block">
            <div class="block-title">
              <div class="block-option pull-right">
                <div class="btn-fb btn-round">
                  
                  <form action="#" method="get">
                    <button type="button" class="btn btn-default btn-round">
                      <i id="searcher-fb-like-box-fb-button" class="fa fa-facebook"></i>
                    </button>
                  </form>
                  
                </div>
              </div>
              <div class="text-center"><h2>Kövess minket</h2></div>
            </div>
            <div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-width="260" data-height="195" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
          </div>
        </div>
        <!-- FB like box end -->
        
      </div>
    </div>
    
    <!-- searcher & fb end -->

    <!-- munka slider -->
    <div class="container">
      <div class="row">
        <div class="label-holder">
          <div class="label label-default label-info label-blue label-big">
            <i class="fa fa-th-large"></i>
            Munkák
          </div>
        </div>
        
        <ul class="slider1">
          
          <!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide -->

          <!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide -->
          
        </ul>
        
        <div class="bx_customNavigation">
          <div id="slider-bx-prev" class="btn prev "></div>
          <div id="slider-bx-next" class="btn next "></div>
        </div>
      </div>
    </div>
    <!-- /munka slider -->
    
    
   
    
     <!-- munkas slider -->
    <div class="container">
      <div class="row">
        <div class="label-holder">
          <div class="label label-default label-info label-blue label-big">
            <i class="fa fa-group"></i>
            Szakemberek
          </div>
          <div class="cs-w-slider-title-left">
            <a href="#">
              <small>Szeretne itt megjelenni?</small>
            </a>
          </div>
        </div>
        
        <ul class="slider2">

          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    
                      <span class="fa fa-star szakember-rate-color-good"></span>
                      <span class="fa fa-star szakember-rate-color-good"></span>
                      <span class="fa fa-star szakember-rate-color-good"></span>
                      <span class="fa fa-star szakember-rate-color-good"></span>
                      <span class="fa fa-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
          <!-- munkás slider elem -->
          <!--
               TODO
               Használd a váltakozó szinekhez a <li class-on az even/odd megoldásokat!!!! %2 == even || odd
               ms-img-odd, ms-img-even
               -->
          <li class="ms-img-odd">
            <div class="cs-item">
              <div class="cs-item-topblock ms-kep">
                <div class="ms-image-holder">
                  <img src="http://lorempixel.com/210/150/animals" alt="" title="" class="img-minied" />
                </div>
                <div class="ms-img-txt">Webdesigner</div>
                <div class="ms-img-txt-place"><small>London</small></div>
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Username
                  </div>
                  <div class="slider-item-rate text-center">
                    <small>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-good"></span>
                      <span class="glyphicon glyphicon-star szakember-rate-color-bad"></span>
                      <span class="slider-item-rate-number"> 4.9</span>
                    </small>
                  </div>
                  <div class="slider-item-rate-btn text-center">
                    <div class="m-slider-wrapper">
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="1" />
                        <button class="glyphicon glyphicon-plus-sign szakember-rate-btn-good btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-plus"><small>65</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="2" />
                        <button class="glyphicon glyphicon-minus-sign szakember-rate-btn-bad btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-minus"><small>5</small></span>
        
                      <form action="#" method="" class="noform">
                        <input type="hidden" name="szakember_rate_id" value="" />
                        <input type="hidden" name="szakember_rate_type" value="3" />
                        <button class="glyphicon glyphicon-record szakember-rate-btn-nope btn-sender"></button>
                      </form>
                      <span class="szakember-rate-txt szakember-rate-nope"><small>3</small></span>
                    </div><!-- /m-slider-wrapper -->
                  </div>
                  <div class="slider-item-rate-msg text-center">
                    (43 vélemény)
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slider item -->
        </ul>
        <div class="bx_customNavigation">
          <div id="slider-bx-prev2" class="btn prev"></div>
          <div id="slider-bx-next2" class="btn next"></div>
        </div>
      </div><!-- /row -->
    </div><!-- /container -->
    <!-- /munkas slider -->
    
    <?php
    print_r($jobs);
    ?>
@stop

@section('scripts')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	{{ HTML::script( URL::asset('js/bootstrap.min.js') ) ; }}
	{{ HTML::script( URL::asset('js/tab.js') ) ; }}
	{{ HTML::script( URL::asset('js/jquery.bxslider.js') ) ; }}
	{{ HTML::script( URL::asset('js/custom.js') ) ; }}
	{{ HTML::script( URL::asset('js/app2.js') ) ; }}
	{{ HTML::script( URL::asset('js/plugins2.js') ) ; }}
	{{ HTML::script( URL::asset('js/bootstrap-slider.js') ) ; }}
	<script>
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&appId=362347407113313&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>

@stop