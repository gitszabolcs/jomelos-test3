@extends('layouts.main')

@section('style')
	{{ HTML::style( URL::asset('css/bootstrap.min.css') ) ; }}
	{{ HTML::style( URL::asset('css/font-awesome-4-1-0.css') ) ; }}
	{{ HTML::style( URL::asset('css/plugins.css') ) ; }}
	{{ HTML::style( URL::asset('css/themes.css') ) ; }}
	{{ HTML::style( URL::asset('css/jquery.bxslider.css') ) ; }}
	{{ HTML::style( URL::asset('css/custom.css') ) ; }}
@stop

@section("content")

<!-- spacer for size: XS, SM, MD -->
    <div class="container">
      <div class="row">
        <div class="navbar-bottom-spacer"></div>
      </div>
    </div>
    <!-- spacer end -->
    
    <!-- tester -->
    <!-- 
    <div class="container">
      <div class="row">
        <div class="col-xs-12 visible-xs">[xs]</div>
        <div class="col-sm-12 visible-sm">[sm]</div>
        <div class="col-md-12 visible-md">[md]</div>
        <div class="col-lg-12 visible-lg">[lg]</div>
      </div>
    </div>
    -->
    <!-- /tester -->
    
    
    
    
   <!-- munka slider -->
    <div class="container">
      <div class="row">
        <div class="label-holder" style="padding-left:5px;">
          <div class="label label-default label-info label-blue label-big">
            <i class="fa fa-th-large"></i>
            Munkák
          </div>
        </div>
        
        <ul class="slider1">
          
          <!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide -->

          <!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide --><!-- slide -->
          <li>
            <div class="cs-item">
              <div class="cs-item-hover-title text-center">
                Szakácsot keresünk lellére, balatonra
              </div>
              <div class="cs-item-topblock">
                <img src="http://lorempixel.com/210/150/people" alt="" title="" class="img-responsive" />
              </div>
              <div class="cs-item-bottom-block">
                <div class="cs-item-bb-left"></div>
                <div class="cs-item-bb-center text-center">
                  <div class="cs-item-bb-txt-head text-center">
                    Kalocsa
                  </div>
                  <div class="cs-item-bb-txt-time text-center">
                      <small><i>Lejár</i>: 2nap 3óra</small>
                  </div>
                  <div class="cs-item-bb-txt-price text-center">
                    40.000 Ft
                  </div>
                </div>
                <div class="cs-item-bb-right">
                  <a href="#">
                    <img src="img/kiskekgomb.png" alt="" title="" />
                  </a>
                </div>
              </div>
            </div>
          </li>
          <!-- /slide -->
          
        </ul>
        
        <div class="bx_customNavigation">
          <div id="slider-bx-prev" class="btn prev "></div>
          <div id="slider-bx-next" class="btn next "></div>
        </div>
      </div>
    </div>
    <!-- /munka slider -->
   
   <!-- munkak lista tabla head kereső --> 
   <div class="container" >
            
            <div class=" munkak-kereso-box my-center " style="max-width: 1110px">
            <div class="row zero-margin">
              <form class="form-inline" method="post" action="#">
          
                <div class="col-sm-3 padding-fix-searcher">
                  <div class="col-xs-12 padding-bottom-10"> Milyen munkát keres? </div>
                  <div class="col-xs-12">
                    <select id="example-chosen-multiple" name="munkak_tags" class="select-chosen" data-placeholder="Kezdjen el gépelni..." multiple>
                      <option value="">Típus 2</option>
                      <option value="">Akármi</option>
                      <option value="">Bármi</option>
                      <option value="">Típus 1</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-3 padding-fix-searcher">
                  <div class="col-xs-12 padding-bottom-10">Hol? </div>
                  <div class="col-xs-12">
                    <select id="example-chosen" name="munkak_tags" class="select-chosen" data-placeholder="Hol?">
                      <option value="">Itt</option>
                      <option value="">Ott</option>
                      <option value="">Amott</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-2 padding-fix-searcher">
                  <div class="col-xs-12 padding-bottom-10"> Munka típusa? </div>
                  <div class="col-xs-12">
                    <select class="select-chosen" name="munkak_tipus">
                      <option value="0">Fix és Órabér</option>
                      <option value="1">Fix ár</option>
                      <option value="2">Órabér</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-3 padding-fix-searcher">
                  <div class="slider_one">
                    <span class="col-xs-12">Órabér</span>
                    <div class="col-xs-12">
                      <input name="search_s_price" id="ex2" type="text" class="span2" value="" data-slider-min="100" data-slider-max="100000" data-slider-step="100" data-slider-value="[12500,45000]" />
                    </div>
                    <div class="col-xs-12 searchbox-gray-2 search-box-mini-titles munkak-searcher-box-slider-titles">
                      <div class="col-xs-6 text-left munkak-searcher-box-text-1"><small>100 Ft</small></div>
                      <div class="col-xs-6 text-right munkak-searcher-box-text-2"><small>100.000 Ft</small></div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-1 padding-fix-searcher">
                  <div class="munkak-kereso-btn-box text-center">
                    <button type="button" class="btn btn-default searcher-box-button jumbo-btn-shadows hidden-md hidden-sm">Keresés</button>
                    <button type="button" class="btn btn-default searcher-box-button jumbo-btn-shadows hidden-lg hidden-xs">
                      <i class="fa fa-search"></i>
                    </button>
                  </div>
                </div>
              </form>
              </div>
            </div>
            <!-- /munkak-kereso-box end -->
            
          </div> <!--/container --> 
    <!-- munkak lista tabla head kereső veege -->
    
    
    
      
    
    
    <!-- munkak tabla -->
    <div class="container">
      
        
        
        <!--
        
            TODO
                
                A table STRIPE (odd||even) csíkozásával nem kell
                foglalkozni, a jQuery script megcsinálja magától!!!!!
        
        -->
        
        <div class="mr-block-right mr-block-holder my-center" style="max-width: 1110px">
            <div class="munka-reszletek-header munka-reszletek-block-head-bg">
                Jelenlegi munkák
            </div>
        
            <div class="table-responsive tab-content-mini">
              <table class="table table-vcenter table-borderless " id="a-munka-tabla">
                <thead>
                  <tr class="white">
                    <th class="text-center"><i class="fa fa-picture-o"></i></th>
                    <th>Cím</th>
                    <th>Szakterület</th>
                    <th class="text-center">Ajánlatok</th>
                    <th class="text-center">Lejár</th>
                    <th class="text-center">Ár</th>
                  </tr>
                </thead>
                
                <tbody>
                  
                  <!-- item -->
                  <tr class=" munkak-table-datarow">
                    <td class="text-center">
                      <img src="http://lorempixel.com/65/65/people" alt="avatar" class="munkak-table-img" />
                    </td>
                    <td>
                      <div class="munkak-table-extrabox">
                        <span class="label label-danger">Sürgős</span>
                        <span class="label label-warning">Kiemelt</span>
                        <span clasS="label label-success">Rejtett</span>
                      </div>
                      <div class="munkak-table-munka-cim">
                        <a href="#">Munka címe - Kőművest keresek kaposvárra</a>
                      </div>
                    </td>
                    <td >Tetőfedő, bádogos</td>
                    <td class="text-center">3</td>
                    <td class="text-center">3nap 8óra</td>
                    <td class="text-center">
                      <div class="label label-success label-love">30.000 Ft</div>
                    </td>
                  </tr>
                  <tr class="munkak-table-hovertext ">
                    <td></td>
                    <td colspan="5">
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                      the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                      type and scrambled it to make a type specimen book.
                    </td>
                  </tr>
                  <!-- /item -->
                  
                  <!-- item -->
                  <tr class=" munkak-table-datarow">
                    <td class="text-center">
                      <img src="http://lorempixel.com/65/65/people" alt="avatar" class="munkak-table-img" />
                    </td>
                    <td>
                      <div class="munkak-table-extrabox">
                        <span class="label label-danger">Sürgős</span>
                        <span class="label label-warning">Kiemelt</span>
                        <span clasS="label label-success">Rejtett</span>
                      </div>
                      <div class="munkak-table-munka-cim">
                        <a href="#">Munka címe - Kőművest keresek kaposvárra</a>
                      </div>
                    </td>
                    <td>Tetőfedő, bádogos</td>
                    <td class="text-center">3</td>
                    <td class="text-center">3nap 8óra</td>
                    <td class="text-center">
                      <div class="label label-success label-love">30.000 Ft</div>
                    </td>
                  </tr>
                  <tr class="munkak-table-hovertext ">
                    <td></td>
                    <td colspan="5">
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                      the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                      type and scrambled it to make a type specimen book.
                    </td>
                  </tr>
                  <!-- /item -->
                  
                  <!-- item -->
                  <tr class=" munkak-table-datarow">
                    <td class="text-center">
                      <img src="http://lorempixel.com/65/65/people" alt="avatar" class="munkak-table-img" />
                    </td>
                    <td>
                      <div class="munkak-table-extrabox">
                        <span class="label label-danger">Sürgős</span>
                        <span class="label label-warning">Kiemelt</span>
                        <span clasS="label label-success">Rejtett</span>
                      </div>
                      <div class="munkak-table-munka-cim">
                        <a href="#">Munka címe - Kőművest keresek kaposvárra</a>
                      </div>
                    </td>
                    <td>Tetőfedő, bádogos</td>
                    <td class="text-center">3</td>
                    <td class="text-center">3nap 8óra</td>
                    <td class="text-center">
                      <div class="label label-success label-love">30.000 Ft</div>
                    </td>
                  </tr>
                  <tr class="munkak-table-hovertext ">
                    <td></td>
                    <td colspan="5">
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                      the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                      type and scrambled it to make a type specimen book.
                    </td>
                  </tr>
                  <!-- /item -->
                  
                  <!-- item -->
                  <tr class=" munkak-table-datarow">
                    <td class="text-center">
                      <img src="http://lorempixel.com/65/65/people" alt="avatar" class="munkak-table-img" />
                    </td>
                    <td>
                      <div class="munkak-table-extrabox">
                        <span class="label label-danger">Sürgős</span>
                        <span class="label label-warning">Kiemelt</span>
                        <span clasS="label label-success">Rejtett</span>
                      </div>
                      <div class="munkak-table-munka-cim">
                        <a href="#">Munka címe - Kőművest keresek kaposvárra</a>
                      </div>
                    </td>
                    <td>Tetőfedő, bádogos</td>
                    <td class="text-center">3</td>
                    <td class="text-center">3nap 8óra</td>
                    <td class="text-center">
                      <div class="label label-success label-love">30.000 Ft</div>
                    </td>
                  </tr>
                  <tr class="munkak-table-hovertext ">
                    <td></td>
                    <td colspan="5">
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                      the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                      type and scrambled it to make a type specimen book.
                    </td>
                  </tr>
                  <!-- /item -->
    
    
                  
                </tbody>
              </table>
            </div><!-- /resp table div end -->
        </div>
    
        <div class="text-center">
          <ul class="pagination pagination-rounded">
            <li><a href="javascript:void(0)">&laquo;</a></li>
            <li class="active"><a href="javascript:void(0)">1</a></li>
            <li><a href="javascript:void(0)">2</a></li>
            <li><a href="javascript:void(0)">3</a></li>
            <li><a href="javascript:void(0)">4</a></li>
            <li><a href="javascript:void(0)">&raquo;</a></li>
          </ul>
        </div>

        
      
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{URL::asset(js/bootstrap.min.js)}}"></script>
    <script src="{{URL::asset(js/tab.js)}}"></script>
    <script src="{{URL::asset(js/jquery.bxslider.js)}}"></script>
    <script src="{{URL::asset(js/custom.js)}}"></script>
    <script src="{{URL::asset(js/app2.js)}}"></script>
    <script src="{{URL::asset(js/plugins2.js)}}"></script>
    <script src="{{URL::asset(js/bootstrap-slider.js)}}"></script>

@stop