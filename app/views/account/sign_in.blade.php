{{ HTML::style( URL::asset('css/bootstrap.min.css') ) ; }}
{{(isset($global)) ? $global : '' }}
<form action="{{ URL::route('account-sign-in-post') }}" method="post" class="form-horizontal"> 

        <div class="field">
                <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputUsername">Username:</label>
                        <div class="col-sm-2">
                                <input class="form-control" type="text" name="username" {{ (Input::old('username')) ? 'value="' . Input::old('username') . '"' : ''}}>
                                @if($errors->has('username'))
                                        {{ $errors->first('username') }}
                                @endif
                        </div>
                </div>
        </div>

        <div class="field">
                <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputPassword">Password:</label>
                        <div class="col-sm-2">
                                <input class="form-control" type="password" name="password">
                                @if($errors->has('password'))
                                        {{ $errors->first('password') }}
                                @endif
                        </div>
                </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-2">
                <input type="submit" value = "Sign in" class="btn btn-primary btn-block">
        </div>
        {{ Form::token() }}
</form>
