@extends('layouts.main')
@section('style')


{{ HTML::style( URL::asset('css/edit.css') ) ; }}

@stop
@section("content")
<p>szia</p>

<div class="page-loading">
   <div class="preloader themed-background">
      <h1 class="push-top-bottom text-light text-center"><strong>Jómelós.hu</strong></h1>
      <div class="inner">
         <h3 class="text-light visible-lt-ie9 visible-lt-ie10"><strong>Loading...</strong></h3>
         <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
      </div>
   </div>
   <!-- spacer for size: XS, SM, MD -->
   <div class="container">
      <div class="row">
         <div class="navbar-bottom-spacer"></div>
      </div>
   </div>
   <!-- spacer end -->
   <!-- Login Full Background -->
   <!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) 
   <img src="/static/img/xjumbo_bg.jpg.pagespeed.ic.CHGunfqu9a.jpg" alt="Login Full Background" class="full-bg animation-pulseSlow" pagespeed_url_hash="1819981593" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
   <!-- END Login Full Background -->
   <!-- Login Container -->
   <div id="login-container" class="animation-fadeIn">
      <!-- Login Title -->
      <div class="login-title text-center">
         <h1><strong>JóMelós.Hu</strong><br>
            <small>Ha jó szakember kell jó áron...</small>
         </h1>
      </div>
      <!-- END Login Title -->
      <!-- Login Block -->
      <div class="block push-bit">
         <!-- Login Form -->
         <form action="#" method="post" id="form-login" class="form-horizontal">
            <input type="hidden" name="redirect" value=""/>
            <div class="form-group">
               <div class="col-xs-12">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                     <input type="text" id="login-email" name="login-email" class="form-control input-lg" placeholder="Email címed vagy felhasználói neved">
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="col-xs-12">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                     <input type="password" id="login-password" name="login-password" class="form-control input-lg" placeholder="Jelszó">
                  </div>
               </div>
            </div>
            <div class="form-group form-actions">
               <div class="col-xs-4">
                  <label class="switch switch-primary" data-toggle="tooltip" title="Jelszó megjegyzése?">
                  <input type="checkbox" id="login-remember-me" name="login-remember-me" checked>
                  <span></span>
                  </label>
               </div>
               <div class="col-xs-8 text-right">
                  <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Bejelentkezés</button>
               </div>
            </div>
            <div class="form-group">
               <div class="col-xs-12 text-center">
                  <a href="javascript:void(0)" id="link-reminder-login"><small>Elfelejtett jelszó?</small></a> -
                  <a href="javascript:void(0)" id="link-register-login"><small>Új fiók létrehozása</small></a>
               </div>
            </div>
         </form>
         <!-- END Login Form -->
         <form action="#default" id="form-recovery-success" class="form-horizontal display-none">
         <div class="alert alert-success alert-dismissable">
            <h4><i class="fa fa-check-circle"></i> Gratulálunk! </h4>
            <p class="text-center">Az új jelszavát beállítottuk<br>
               Pár másodperc múlva átirányítjuk...
            </p>
         </div>
         </form>
         <form action="#default" id="form-recovery-new-password" class="form-horizontal display-none">
         <input type="hidden" name="recovery-email" id="recovery-email" value="">
         <input type="hidden" name="recovery-token" id="recovery-token" value="">
         <div class="alert alert-success alert-dismissable">
            <h4><i class="fa fa-check-circle"></i> Siker! </h4>
            Kérjük írja be az új, ön által válaszott jelszavakat.
         </div>
         <div class="form-group">
            <div class="col-xs-12">
               <div class="input-group">
                  <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                  <input type="password" id="recovery-password" name="recovery-password" class="form-control input-lg" placeholder="Jelszó">
               </div>
            </div>
         </div>
         <div class="form-group">
            <div class="col-xs-12">
               <div class="input-group">
                  <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                  <input type="password" id="recovery-password-verify" name="recovery-password-verify" class="form-control input-lg" placeholder="Jelszó megerősítése">
               </div>
            </div>
         </div>
         <div class="form-group form-actions">
            <div class="col-xs-4 text-right col-xs-offset-7">
               <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Beállítom</button>
            </div>
         </div>
         </form>
         <form action="#default" id="form-recovery-sent" class="form-horizontal display-none">
         <div class="alert alert-success alert-dismissable">
            <h4><i class="fa fa-check-circle"></i> Gratulálunk! </h4>
            Az új jelszavához szükséges kódot kiküldtük az emailcímére.<br>
            Kérjük ellenőrizze az email címét majd az ott található kódot másolja be a lentebb található dobozba.
         </div>
         <div class="form-group">
            <div class="col-xs-6 col-xs-offset-1">
               <div class="input-group">
                  <span class="input-group-addon"><i class="gi gi-keys"></i></span>
                  <input type="text" id="auth-code" name="auth-code" class="form-control input-lg" placeholder="Kód helye">
               </div>
            </div>
            <div class="col-xs-4">
               <button type="submit" class="btn btn-lg btn-success "><i class="fa fa-plus"></i> Hitelesítés</button>
            </div>
         </div>
         </form>
         <form action="login_full.html#reminder" method="post" id="form-reminder" class="form-horizontal form-bordered form-control-borderless display-none">
            <div class="form-group">
               <div class="col-xs-12">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                     <input type="text" id="reminder-email" name="reminder-email" class="form-control input-lg" placeholder="Email">
                  </div>
               </div>
            </div>
            <div class="form-group form-actions">
               <div class="col-xs-12 text-right">
                  <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Új jelszó</button>
               </div>
            </div>
            <div class="form-group">
               <div class="col-xs-12 text-center">
                  <small>Tudod a jelszavad?</small> <a href="javascript:void(0)" id="link-reminder"><small>Bejelentkezés</small></a>
               </div>
            </div>
         </form>
         <!-- END Reminder Form -->
         <form action="#default" id="form-auth-success" class="form-horizontal display-none">
         <div class="alert alert-success alert-dismissable">
            <h4><i class="fa fa-check-circle"></i> Gratulálunk! </h4>
            A kód ellenőrzése sikeres, pár másodperc múlva átirányítjuk.
         </div>
         </form>
         <form action="#default" id="form-register-success" class="form-horizontal display-none">
         <div class="alert alert-success alert-dismissable">
            <h4><i class="fa fa-check-circle"></i> Gratulálunk! </h4>
            Sikeresen regisztrált a Jómelós.hu weboldalra.<br>
            Kérjük ellenőrizze az email címét majd az ott található kódot másolja be a lentebb található dobozba.
         </div>
         <div class="form-group">
            <div class="col-xs-6 col-xs-offset-1">
               <div class="input-group">
                  <span class="input-group-addon"><i class="gi gi-keys"></i></span>
                  <input type="text" id="auth-code" name="auth-code" class="form-control input-lg" placeholder="Kód helye">
               </div>
            </div>
            <div class="col-xs-4">
               <button type="submit" class="btn btn-lg btn-success "><i class="fa fa-plus"></i> Hitelesítés</button>
            </div>
         </div>
         </form>
         <!-- Register Form -->
         <form action="{{URL::route('account-sign-up-post')}}" method="post" id="form-register" class="form-horizontal display-none">
            <div class="form-group">
               <div class="col-xs-6">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="gi gi-user"></i></span>
                     <input type="text" id="register-lastname" name="register-lastname" class="form-control input-lg" placeholder="Vezetéknév">
                  </div>
               </div>
               <div class="col-xs-6">
                  <input type="text" id="register-firstname" name="register-firstname" class="form-control input-lg" placeholder="Keresztnév">
               </div>
            </div>
            <div class="form-group">
               <div class="col-xs-12">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="gi gi-user_add"></i></span>
                     <input type="text" id="register-username" name="register-username" class="form-control input-lg" placeholder="Felhasználó név">
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="col-xs-12">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="gi gi-earphone"></i></span>
                     <input type="text" id="register-phone" name="register-phone" class="form-control input-lg" placeholder="Telefonszám">
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="col-xs-12">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                     <input type="text" id="register-email" name="register-email" class="form-control input-lg" placeholder="Email">
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="col-xs-12">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                     <input type="password" id="register-password" name="register-password" class="form-control input-lg" placeholder="Jelszó">
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="col-xs-12">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                     <input type="password" id="register-password-verify" name="register-password-verify" class="form-control input-lg" placeholder="Jelszó megerősítése">
                  </div>
               </div>
            </div>
            <div class="form-group form-actions">
               <div class="col-xs-8">
                  <a href="#modal-terms" data-toggle="modal" class="register-terms">Felhasználói feltételek</a>
                  <label class="switch switch-primary" data-toggle="tooltip" title="Elfogadom a felhasználói feltételeket">
                  <input type="checkbox" id="register-terms" name="register-terms">
                  <span></span>
                  </label>
               </div>
               <div class="col-xs-4 text-right">
                  <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Regisztrálás</button>
               </div>
            </div>
            <div class="form-group">
               <div class="col-xs-12 text-center">
                  <small>Van már számlája?</small> <a href="javascript:void(0)" id="link-register"><small>Bejelentkezés</small></a>
               </div>
            </div>
         </form>
         <!-- END Register Form -->
      </div>
      <!-- END Login Block -->
   </div>
   <!-- END Login Container -->
   <!-- Modal Terms -->
   <div id="modal-terms" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
               <h4 class="modal-title">Szabályzat &amp; felhasználói feltételek</h4>
            </div>
            <div class="modal-body">
               <h4>Title</h4>
               <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
               <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
               <h4>Title</h4>
               <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
               <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
               <h4>Title</h4>
               <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
               <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
         </div>
      </div>
   </div>
</div>
@stop
@section('scripts')
{{----}}
	{{ HTML::script( URL::asset('js/custom.js') ) ; }}
   {{ HTML::script( URL::asset('js/app2.js') ) ; }}
   {{ HTML::script( URL::asset('js/plugins.js') ) ; }}
   {{ HTML::script( URL::asset('js/plugins2.js') ) ; }}
	{{ HTML::script( URL::asset('js/plugins3.js') ) ; }}
   {{ HTML::script( URL::asset('js/bootstrap-slider.js') ) ; }}
   {{ HTML::script( URL::asset('js/app.js') ) ; }}
   {{ HTML::script( URL::asset('js/_.js') ) ; }}

			<script>
        $(function(){ 
            Jomelos.Init();
            Jomelos.login(); 
        });
        </script>
@stop


