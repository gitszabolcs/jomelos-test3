<div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-fixed-top navbar-default navbar-jomelos" role="navigation">
      <div class="container">
        <div class="navbar-header">
          
          <!-- menu button for toggled content -->
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <!-- BRAND LOGO -->
          <a class="navbar-brand brand" href="http://edesizek.eu/">
            <!-- Jómelós.hu-->
            {{ HTML::image('img/logo_new.png','jomelos.hu',array('class' => 'hidden-xs')) }}
            <!--<img src="img/logo_new.png" alt="" title="" class="hidden-xs" />-->
            <div class="visible-xs">Jómelós.hu</div>
          </a>
          <!-- end of BRAND LOGO -->
          
        </div>
        
        <!-- menu points -->
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="http://edesizek.eu/">Főoldal</a></li>
            <li><a href="../munkak">Munkák</a></li>
            <li><a href="../szakemberek">Szakemberek</a></li>
          </ul>
          
          <!-- right side of navbar -->
          <ul class="nav navbar-nav navbar-right">
            <li class="hidden-xs">
              <form method="get" action="#">
                <a href="{{URL::route('get-sign-up')}}#register"><button type="button" class="btn navbar-btn btn-sm btn-green">
                  <span class="glyphicon glyphicon-plus"></span>
                    Regisztráció
                </button></a>
              </form>
            </li>
            <li class="hidden-xs">
              <form method="get" action="#">
                <button type="button" class="btn navbar-btn btn-sm btn-blue">
                  <span class="glyphicon glyphicon-user"></span>
                  Login
                </button>
              </form>
            </li>
            
            <li class="visible-xs">
              <a href="#">Regisztráció</a>
            </li>
            <li class="visible-xs">
              <a href="#">Login</a>
            </li>
            
          </ul>
          <!-- end of right side -->
          
        </div>
      </div>
    </div>
  </div>
</div>