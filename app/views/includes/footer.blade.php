<!-- footer -->
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 footer-dark-spacer">
          
        </div>
      </div>
    </div>
    <!-- <footer>-->
      <div class="container-fluid">
        <div class="row">

        <!-- Quick Stats -->
            <section class="site-content site-section themed-background">
                <div class="container">
                    <!-- Stats Row -->
                    <!-- CountTo (initialized in js/app.js), for more examples you can check out https://github.com/mhuggins/jquery-countTo -->
                    <div class="row" id="counters">
                        <div class="col-sm-3">
                            <div class="counter site-block">
                                <span data-toggle="countTo" data-to="6800" data-after="+"></span>
                                <small>Regisztrált tagok</small>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="counter site-block">
                                <span data-toggle="countTo" data-to="5500" data-after="+"></span>
                                <small>Regisztrált szakember</small>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="counter site-block">
                                <span data-toggle="countTo" data-to="12352" data-after=""></span>
                                <small>Összes beküldött munka</small>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="counter site-block">
                                <span data-toggle="countTo" data-to="8500" data-after=""></span>
                                <small>Aktív munkák</small>
                            </div>
                        </div>
                    </div>
                    <!-- END Stats Row -->
                </div>
            </section>
            <!-- END Quick Stats -->
        
            <!-- Footer -->
            <footer class="site-footer">
                <div class="container-fluid">
                  <div class="container">
                    <!-- Footer Links -->
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <h4 class="footer-heading">Rólunk</h4>
                            <ul class="footer-nav list-inline">
                                <li><a href="contact.html">Kapcsolat</a></li>
                                <li><a href="contact.html">Általános szerződési feltételek</a></li>
                                <li><a href="contact.html">Jogi nyilatkozat</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h4 class="footer-heading">Segítség</h4>
                            <ul class="footer-nav list-inline">
                                <li><a href="javascript:void(0)">Megbízóknak</a></li>
                                <li><a href="javascript:void(0)">Szakembereknek</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h4 class="footer-heading">Kövess minket</h4>
                            <ul class="footer-nav footer-nav-social list-inline">
                                <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h4 class="footer-heading"><span id="year-copy">2014</span> &copy; <a href="#">JÓMELÓS.HU</a></h4>
                            <ul class="footer-nav list-inline">
                                <li>Az online munkaerőpiac</li>
                            </ul>
                        </div>
                    </div>
                    <!-- END Footer Links -->
                  </div>
                </div>
            </footer>
            <!-- END Footer -->

            <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
            <a href="#" id="to-top"><i class="fa fa-angle-up"></i></a>
            
        </div>
      </div>
    <!-- </footer>-->