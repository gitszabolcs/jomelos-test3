<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Jomelós</title>
    {{ HTML::style( URL::asset('css/bootstrap.min.css') ) ; }}
    {{ HTML::style( URL::asset('css/font-awesome-4-1-0.css') ) ; }}
    {{ HTML::style( URL::asset('css/plugins.css') ) ; }}
    {{ HTML::style( URL::asset('css/themes.css') ) ; }}
    {{ HTML::style( URL::asset('css/jquery.bxslider.css') ) ; }}
    {{ HTML::style( URL::asset('css/custom.css') ) ; }}
    {{ HTML::style( URL::asset('css/edit.css') ) ; }}
    {{ HTML::style( URL::asset('plugins/fancybox/jquery.fancybox.css') ) ; }}
</head>
<body>
    @yield('content')
</body>
<footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    {{ HTML::script( URL::asset('js/bootstrap.min.js') ) ; }}
    {{ HTML::script( URL::asset('js/tab.js') ) ; }}
    {{ HTML::script( URL::asset('js/jquery.bxslider.js') ) ; }}
    {{ HTML::script( URL::asset('js/custom.js') ) ; }}
    {{ HTML::script( URL::asset('js/app2.js') ) ; }}
    {{ HTML::script( URL::asset('js/plugins.js') ) ; }}
    {{ HTML::script( URL::asset('js/plugins2.js') ) ; }}
    {{ HTML::script( URL::asset('js/plugins3.js') ) ; }}
    {{ HTML::script( URL::asset('js/bootstrap-slider.js') ) ; }}
    {{ HTML::script( URL::asset('js/_.js') ) ; }}
</footer>
</html>