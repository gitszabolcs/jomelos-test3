<div role="navigation" class="navbar navbar-fixed-top navbar-default navbar-jomelos">
    <div class="container">
      <div class="navbar-header">

        <!-- menu button for toggled content -->
        <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <!-- BRAND LOGO -->
        <a href="/" class="navbar-brand brand">
          <!-- Jómelós.hu-->
          <img title="" alt="" src="/static/img/logo_new.png">
          <!--<div class="visible-xs">Jómelós.hu</div>-->
        </a>
        <!-- end of BRAND LOGO -->

      </div>

      <!-- menu points -->
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><a href="/">Főoldal</a></li>
          <li><a href="/munkak">Munkák</a></li>
          <li><a href="/szakemberek">Szakemberek</a></li>
        </ul>

                      <!-- right side of navbar -->
        <ul class="nav navbar-nav navbar-right">
          <li class="hidden-xs">
            <a href="http://dev.jomelos.com/bejelentkezes#register"> 
                <span class="glyphicon glyphicon-plus"></span>
                Regisztráció
            </a>
          </li>
          <li class="hidden-xs">
            <a href="http://dev.jomelos.com/bejelentkezes"> 
                <span class="glyphicon glyphicon-user"></span>
                Bejelentkezés
            </a>
          </li>

          <li class="visible-xs">
            <a href="http://dev.jomelos.com/bejelentkezes#register">Regisztráció</a>
          </li>
          <li class="visible-xs">
            <a href="http://dev.jomelos.com/bejelentkezes">Bejelentkezés</a>
          </li>

        </ul>
                      <!-- end of right side -->

      </div>
    </div>
</div>